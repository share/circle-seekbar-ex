package com.example.roundprogressbar;

import com.example.circlepregress.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * 仿iphone带进度的进度条，线程安全的View，可直接在线程中更新进度
 * 
 * @author xiaanming
 * @author dangsang
 * @since 2014.7.4
 * @version 2.0 Add seek thumb control
 * 
 */
public class RoundProgressBar extends View {
	/** * 画笔对象的引用 */
	private Paint paint = new Paint();
	PointF point = new PointF();
	PointF point2 = new PointF();
	/** Circle rect */
	RectF oval = new RectF();
	/** Seek bar changed lisener */
	OnSeekChangeListener mListener;

	/** 圆环的颜色 */
	private int roundColor;

	/** 圆环进度的颜色 */
	private int roundProgressColor;

	/** 中间进度百分比的字符串的颜色 */
	private int textColor;

	/** 中间进度百分比的字符串的字体 */
	private float textSize;

	/** 圆环的宽度 */
	private float roundWidth;
	/** The max length of thumb */
	private float maxThumb = 40;
	private float thumbX, thumbY;

	/** 最大进度 */
	private int max;

	/** 当前进度 */
	private int progress;
	/** 是否显示中间的进度 */
	private boolean textIsDisplayable;
	/** Start angle */
	private int angle;

	/** 进度的风格，实心或者空心 */
	private int style;

	public static final int STROKE = 0;
	public static final int FILL = 1;

	protected float centre;
	protected float radius;
	/**
	 * The adjustment factor. This adds an adjustment of the specified size to
	 * both sides of the progress bar, allowing touch events to be processed
	 * more user friendlily (yes, I know that's not a word)
	 */
	private int factor = 100;
	/** When touch is in sensor rect, moving is available */
	boolean movable = false;
	int thumb;
	Bitmap thumbBit;

	public void setAdjustmentFactor(int adjustmentFactor) {
		this.factor = adjustmentFactor;
	}

	/**
	 * The listener interface for receiving onSeekChange events. The class that
	 * is interested in processing a onSeekChange event implements this
	 * interface, and the object created with that class is registered with a
	 * component using the component's
	 * <code>setSeekBarChangeListener(OnSeekChangeListener)<code> method. When
	 * the onSeekChange event occurs, that object's appropriate
	 * method is invoked.
	 * 
	 * @see OnSeekChangeEvent
	 */
	public interface OnSeekChangeListener {

		/**
		 * On progress change.
		 * 
		 * @param view
		 *            the view
		 * @param newProgress
		 *            the new progress
		 */
		public void onProgressChanged(RoundProgressBar view, int progress);

		public void onStopTrackingTouch(RoundProgressBar view);
	}

	{
		mListener = new OnSeekChangeListener() {

			@Override
			public void onProgressChanged(RoundProgressBar view, int newProgress) {
			}

			@Override
			public void onStopTrackingTouch(RoundProgressBar view) {
			}
		};
	}

	public void setChangeListener(OnSeekChangeListener mListener) {
		this.mListener = mListener;
	}

	public RoundProgressBar(Context context) {
		this(context, null);
	}

	public RoundProgressBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public RoundProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray mTypedArray = context.obtainStyledAttributes(attrs,
				R.styleable.RoundProgressBar);

		// 获取自定义属性和默认值
		roundColor = mTypedArray.getColor(
				R.styleable.RoundProgressBar_roundColor, Color.TRANSPARENT);
		roundProgressColor = mTypedArray.getColor(
				R.styleable.RoundProgressBar_roundProgressColor, Color.GREEN);
		textColor = mTypedArray.getColor(
				R.styleable.RoundProgressBar_textColor, Color.GREEN);
		textSize = mTypedArray.getDimension(
				R.styleable.RoundProgressBar_textSize, 15);
		roundWidth = mTypedArray.getDimension(
				R.styleable.RoundProgressBar_roundWidth, 5);
		factor = (int) mTypedArray.getDimension(
				R.styleable.RoundProgressBar_factor, 40);
		max = mTypedArray.getInteger(R.styleable.RoundProgressBar_max, 100);
		textIsDisplayable = mTypedArray.getBoolean(
				R.styleable.RoundProgressBar_textIsDisplayable, false);
		style = mTypedArray.getInt(R.styleable.RoundProgressBar_style, 0);
		progress = mTypedArray.getInt(R.styleable.RoundProgressBar_progress, 0);
		angle = mTypedArray.getInt(R.styleable.RoundProgressBar_angle, -90);
		thumb = mTypedArray
				.getResourceId(R.styleable.RoundProgressBar_thumb, 0);
		if (thumb != 0)
			thumbBit = BitmapFactory.decodeResource(context.getResources(),
					thumb);

		mTypedArray.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (thumb != 0)
			maxThumb = thumbBit.getWidth();
		centre = getWidth() / 2; // 获取圆心的x坐标
		radius = (centre - roundWidth / 2 - maxThumb / 2); // 圆环的半径
		initThumbPos();
	}

	// 初始化thumb的位置
	void initThumbPos() {
		double ang = 2.0 * Math.PI * progress / max;
		ratioMoving(centre * Math.sin(ang) + centre,
				centre - centre * Math.cos(ang));
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		centre = getWidth() / 2; // 获取圆心的x坐标
		radius = (centre - roundWidth / 2 - maxThumb / 2); // 圆环的半径

		/** * 画最外层的大圆环 */
		paint.setColor(roundColor); // 设置圆环的颜色
		paint.setStyle(Paint.Style.STROKE); // 设置空心
		paint.setStrokeWidth(roundWidth); // 设置圆环的宽度
		paint.setAntiAlias(true); // 消除锯齿
		canvas.drawCircle(centre, centre, radius, paint); // 画出圆环
		//Log.e("log", centre + "");

		/** * 画进度百分比 */
		paint.setStrokeWidth(0);
		paint.setColor(textColor);
		paint.setTextSize(textSize);
		paint.setTypeface(Typeface.DEFAULT_BOLD); // 设置字体
		int percent = (int) (((float) progress / (float) max) * 100); // 中间的进度百分比，先转换成float在进行除法运算，不然都为0
		float textWidth = paint.measureText(percent + "%"); // 测量字体宽度，我们需要根据字体的宽度设置在圆环中间
		if (textIsDisplayable && style == STROKE) {// && percent != 0 ) {
			canvas.drawText(percent + "%", centre - textWidth / 2, centre
					+ textSize / 2, paint); // 画出进度百分比
		}

		/** * 画圆弧 ，画圆环的进度 */
		// 设置进度是实心还是空心
		paint.setStrokeWidth(roundWidth); // 设置圆环的宽度
		paint.setColor(roundProgressColor); // 设置进度的颜色
		oval.set(centre - radius, centre - radius, centre + radius, centre
				+ radius); // 用于定义的圆弧的形状和大小的界限
		if (style == STROKE) {
			paint.setStyle(Paint.Style.STROKE);
			canvas.drawArc(oval, angle, 360 * progress / max, false, paint); // 根据进度画圆弧
		} else if (style == FILL) {
			paint.setStyle(Paint.Style.FILL_AND_STROKE);
			// if (progress != 0)
			canvas.drawArc(oval, angle, 360 * progress / max, true, paint); // 根据进度画圆弧
		}

		drawThumb(canvas);
	}

	private void drawThumb(Canvas canvas) {
		paint.setStyle(Paint.Style.FILL);
		if (thumb != 0)
			canvas.drawBitmap(thumbBit, thumbX - maxThumb / 2, thumbY
					- maxThumb / 2, null);
		else
			canvas.drawCircle(thumbX, thumbY, maxThumb / 2, paint); // 画出圆环
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (inThumbRect(x, y))
				movable = true;
			// moved(x, y, up);
			// ratioMoving(x, y);
			// postInvalidate();
			// sent change mess
			break;
		case MotionEvent.ACTION_MOVE:
			// one more do not over line
			if (movable) {
				// if(inThumbRect(x, y)){
				ratioMoving(x, y);
				progressChange(x, y);
				// sent changing mess
				mListener.onProgressChanged(this, progress);
			}
			break;
		case MotionEvent.ACTION_UP:
			// moved(x, y, up);
			// sent stop tracking touch
			if (movable) {
				mListener.onStopTrackingTouch(this);
				movable = false;
			}
			break;
		}
		return true;
	}

	/** Active thumb */
	boolean inThumbRect(float x, float y) {
		oval.set(thumbX - factor, thumbY - factor, thumbX + factor, thumbY
				+ factor);
		return oval.contains(x, y);
	}

	/** Progress change */
	void progressChange(float x, float y) {
		int newp = (int) (max * (angleBetweenThreePoints(x, y) / 2.0 / Math.PI));
		boolean fast = false;
		// When big gap occured, repos
		if (progress > max / 2 && newp < max / 2) {
			setProgress(max);
			fast = true;
		}
		if (progress < max / 2 && newp > max / 2) {
			setProgress(0);
			fast = true;
		}
		if (!fast)
			setProgress(newp);
	}

	/**
	 * Calculate thumb position using touch point,they are at one line across
	 * circle
	 */
	void ratioMoving(double x, double y) {
		double k = radius
				/ Math.sqrt((x - centre) * (x - centre) + (y - centre)
						* (y - centre));
		thumbX = (float) (centre + k * (x - centre));
		thumbY = (float) (centre + k * (y - centre));
		//Log.e("call", "" + thumbX + "(-------)" + thumbY + "/" + (x - centre));
	}

	public synchronized int getMax() {
		return max;
	}

	/**
	 * 设置进度的最大值
	 * 
	 * @param max
	 */
	public synchronized void setMax(int max) {
		if (max < 0) {
			throw new IllegalArgumentException("max not less than 0");
		}
		this.max = max;
		initThumbPos();
	}

	/**
	 * 获取进度.需要同步
	 * 
	 * @return
	 */
	public synchronized int getProgress() {
		return progress;
	}

	/**
	 * 设置进度，此为线程安全控件，由于考虑多线的问题，需要同步 刷新界面调用postInvalidate()能在非UI线程刷新
	 * 
	 * @param progress
	 */
	public synchronized void setProgress(int progress) {
		if (progress < 0) {
			throw new IllegalArgumentException("progress not less than 0");
		}
		if (progress > max) {
			progress = max;
		}
		if (progress <= max) {
			this.progress = progress;
			initThumbPos();
			postInvalidate();
		}

	}

	public int getCircleColor() {
		return roundColor;
	}

	public void setCircleColor(int CircleColor) {
		this.roundColor = CircleColor;
	}

	public int getCircleProgressColor() {
		return roundProgressColor;
	}

	public void setCircleProgressColor(int CircleProgressColor) {
		this.roundProgressColor = CircleProgressColor;
	}

	public int getTextColor() {
		return textColor;
	}

	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}

	public float getTextSize() {
		return textSize;
	}

	public void setTextSize(float textSize) {
		this.textSize = textSize;
	}

	public float getRoundWidth() {
		return roundWidth;
	}

	public void setRoundWidth(float roundWidth) {
		this.roundWidth = roundWidth;
	}

	double angleBetweenThreePoints(float x, float y) {
		point.set(centre - centre + maxThumb / 2, centre * 2 - centre);
		point2.set(x - centre + maxThumb / 2, y - centre);

		double angle2 = Math.atan2(point2.x * point.y - point.x * point2.y,
				point.x * point2.x + point.y * point2.y);
		//Log.e("can", "----)" + (Math.PI - angle2) + "/" + Math.PI);
		return (Math.PI - angle2);
	}

}
